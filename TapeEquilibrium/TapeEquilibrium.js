function solution(A) {
    let P = A.length - 1;
    let temp = null;
    let first = 0;   
    
    const sum = A.slice(0, A.length).reduce(getSum); //get sum
     
    for(let i=0; i < P ; i++){
        
        first += A[i];
        second = sum - first; //to get the sum of the second tape, just get the difference fo the sum of first
        
        let difference = Math.abs(first-second);

        if(temp == null){
            temp = difference;
        }
        
        if(difference < temp){
            temp = difference;
        }
        
    }
    return temp;
}
function getSum(total, num) {
    return total + num;
}