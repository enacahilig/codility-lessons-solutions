
function solution(A, K) {
    if(A.length == 0){
    	return A;
    }
    let rotatedArray = A;
    for(i=0; i<K; i++){ //loop k times
        rotatedArray = rotate(rotatedArray);
    }
    return rotatedArray;
}

function rotate(rotatedArray){
    let element = rotatedArray.pop(); //remove last element
    rotatedArray.unshift(element); // add removed element to the first index of the array
    return rotatedArray;
}