function solution(A) {
    A.sort(function(a, b){return a - b}); //sort min to max
    let temp = [];
    for(let i=0; i < A.length; i++){
        temp[A[i]] = true; //create a hash with keys as the values in arrya
    }
    
    let i = 1;
    while (true){ 
        if(typeof(temp[i]) == "undefined"){ //return the key of the first undefined lement
            return i;
        }
        i++;
    }
   
}