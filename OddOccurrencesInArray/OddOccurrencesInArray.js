function solution(A) {
    //sort min to max
    const temp = A.sort(function(a, b){return a - b});

    let counter = 1;
    let prev = temp[0];

    for(let i=1; i < temp.length; i++){
        if(prev == temp[i]){
            counter++; //count if the current number is equal to the previous
        }
        else{
            //if number is changed, check if counter is odd or even
            if(counter % 2 == 0){
                prev = temp[i]; //if even, set prev to the current number and reset counter
                counter = 1;
            }
            else{
                break; 
            }
           
        }
    }
    return prev; 
}