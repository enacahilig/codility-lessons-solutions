function solution(N) {
    let binary = N.toString(2);
    
    binary = binary.split(1);
    let gap = 0;
    
    if(binary.length < 3){ //if no gap
        return gap;
    }
    
    for(let i=0;i<binary.length;i++){
        let before = typeof(binary[i-1]) != "undefined" ?  true : false;
        let after =  typeof(binary[i+1]) != "undefined" ?  true : false;
        if(binary[i].length > gap && before && after ){ 
            gap = binary[i].length;  //set the longest gap
        }
    }
    return gap;
}