function solution(X, Y, D) {    
    //get how many jumps

    let jumps = Math.ceil( (Y-X) / D);
    return jumps;
}